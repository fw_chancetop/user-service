package api.learncorengservice;

import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

public interface LearnCoreNGWebService {
    @GET
    @Path("/:s")
    void practiceUtility(@PathParam("s") String s);
}
