package app.otherservice.service;

import app.learncorengservice.LearnCoreNGModule;
import core.framework.module.App;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OtherWebService extends App {
    private final Logger logger = LoggerFactory.getLogger(OtherWebService.class);

    @Override
    protected void initialize() {
        load(new LearnCoreNGModule());
        logger.info("Other web service loaded");
    }
}
