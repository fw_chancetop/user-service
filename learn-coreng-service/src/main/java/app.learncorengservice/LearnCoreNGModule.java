package app.learncorengservice;

import api.learncorengservice.LearnCoreNGWebService;
import app.learncorengservice.service.LearnCoreNGService;
import app.learncorengservice.web.LearnCoreNGServiceImpl;
import core.framework.module.Module;

public class LearnCoreNGModule extends Module {
    @Override
    protected void initialize() {
        var service = bind(LearnCoreNGService.class);
        service.practiceUtilty("  in learnCoreNGModule    ");
        api().service(LearnCoreNGWebService.class, bind(LearnCoreNGServiceImpl.class));
    }
}
