package app.learncorengservice.service;


import core.framework.module.App;
import core.framework.module.SystemModule;
import core.framework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LearnCoreNGService extends App {
    private final Logger logger = LoggerFactory.getLogger(LearnCoreNGService.class);

    public void practiceUtilty(String s) {
        logger.info("Output: " + Strings.strip(s));
    }

    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        practiceUtilty(" abc ");
    }
}
