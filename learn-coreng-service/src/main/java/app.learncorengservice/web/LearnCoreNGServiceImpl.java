package app.learncorengservice.web;

import api.learncorengservice.LearnCoreNGWebService;
import app.learncorengservice.service.LearnCoreNGService;
import core.framework.inject.Inject;

public class LearnCoreNGServiceImpl implements LearnCoreNGWebService {
    @Inject
    LearnCoreNGService learnCoreNGService;

    @Override
    public void practiceUtility(String s) {
        learnCoreNGService.practiceUtilty(s);
    }
}
